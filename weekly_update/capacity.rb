# Queries a Google App Script which exposes aggregate team PTO from Google Calendar
class Capacity
  attr_reader :today

  def initialize(today)
    @today = today
  end

  def response
    @response ||= response = HTTParty.get(capacity_url)
  end

  # Use next week to get a forward-looking capacity
  def capacity_url
    Team.capacity_url + "&date=#{today.strftime('%Y-%m-%d')}"
  end

  def days_in_period
    response['days']
  end

  def frontend_pto
    response['frontend']
  end

  def backend_pto
    response['backend']
  end

  def frontend_days
    days_in_period * Team.frontend.length
  end

  def backend_days
    days_in_period * Team.backend.length
  end

  def engineer_days
    frontend_days + backend_days
  end

  def frontend_borrow
    Team.frontend_borrow * days_in_period
  end

  def backend_borrow
    Team.backend_borrow * days_in_period
  end

  def frontend_deficit
    frontend_pto + frontend_borrow
  end

  def backend_deficit
    backend_pto + backend_borrow
  end

  def team_deficit
    backend_deficit + frontend_deficit
  end

  def team_total
    engineer_days - team_deficit
  end

  def frontend
    frontend_days - frontend_deficit
  end

  def backend
    backend_days - backend_deficit
  end
end