class Description
  attr_reader :weekly_update, :template_markdown, :plan

  SECTIONS = {
    'navigation': :navigation_markdown,
    'capacity': :capacity_markdown,
    'error-budget': :error_budget_markdown,
    'issue-distribution': :issue_distribution_markdown,
    'progress-summary': :progress_summary_markdown,
    'progress-detail': :progress_detail_markdown,
    'team-activity': :team_activities_markdown,
    'rollover': :rollover_issues_markdown,
    'team-links': :team_links_markdown,
    'quick-actions': :quick_actions_markdown,
    'highlights': :highlights_markdown
  }

  def self.skip_sections(skipped)
    raise "Invalid section in #{skipped}" unless (skipped - SECTIONS.keys).length == 0
    @@sections = SECTIONS.reject { skipped.include?(_1) }
  end

  def initialize(weekly_update, template_markdown)
    @weekly_update = weekly_update
    @plan = weekly_update.plan
    @template_markdown = template_markdown
  end

  def render(action)
    unwrap_threads(template_markdown) if action == :UPDATE
    @@sections.each do |section, method|
      upcase_section = section.to_s.upcase
      puts "\nStarting generation for #{upcase_section} section...\n"
      next unless template_markdown.match?(/<!-- REPLACE-WITH-#{upcase_section}(-END|-START)?:#{action} -->/)

      result = send(method).to_s

      while range = get_block_range(template_markdown, upcase_section, action)
        template_markdown[range] = result
      end
    end

    wrap_threads(template_markdown) if action == :CREATE
    collect_threads!(template_markdown, action)

    template_markdown
  end

  def get_block_range(markdown, section, action)
    oneliner = "<!-- REPLACE-WITH-#{section}:#{action} -->"

    if i = markdown.index(oneliner)
      return (i...(i + oneliner.length))
    end

    start_text = "<!-- REPLACE-WITH-#{section}-START:#{action} -->"
    end_text = "<!-- REPLACE-WITH-#{section}-END:#{action} -->"

    return unless j = markdown.index(start_text)
    return unless k = markdown.index(end_text)

    (j...(k + end_text.length))
  end

  def get_content_range(markdown, section, action)
    start_text = "<!-- REPLACE-WITH-#{section}-START:#{action} -->"
    end_text = "<!-- REPLACE-WITH-#{section}-END:#{action} -->"

    return unless j = markdown.index(start_text)
    return unless k = markdown.index(end_text)

    ((j + start_text.length)...k)
  end

  def collect_threads!(markdown, action)
    while range = get_block_range(markdown, :THREAD, action)
      comment_range = get_content_range(markdown[range], :THREAD, action)

      weekly_update.add_thread(markdown[range][comment_range])

      markdown[range] = ''
    end
  end

  def wrap_threads(markdown)
    while range = get_block_range(markdown, :THREAD, :UPDATE)
      comment_range = get_content_range(markdown[range], :THREAD, :UPDATE)
      comment = markdown[range][comment_range]

      comment = comment.gsub('"', '&quot;')
      comment = comment.split("\n").join("\\\n")

      comment = <<~MARKDOWN
      <!-- REPLACE-WITH-HIDDEN-START:UPDATE -->
      <div title=\"\\\n#{comment}\n\"\/>
      <!-- REPLACE-WITH-HIDDEN-END:UPDATE -->
      MARKDOWN

      markdown[range] = comment
    end
  end

  def unwrap_threads(markdown)
    while range = get_block_range(markdown, :HIDDEN, :UPDATE)
      comment_range = get_content_range(markdown[range], :HIDDEN, :UPDATE)
      comment = markdown[range][comment_range]

      comment = comment.gsub(/^<div title=\"\\\n/, '')
      comment = comment.gsub(/\n\"\/>$/, '')
      comment = comment.split("\\\n").join("\n")
      comment = comment.gsub('&quot;', '"')

      comment = <<~MARKDOWN
      <!-- REPLACE-WITH-THREAD-START:UPDATE -->
      #{comment}
      <!-- REPLACE-WITH-THREAD-END:UPDATE -->
      MARKDOWN

      markdown[range] = comment
    end
  end

  def navigation_markdown
    nav_items = []

    if weekly_update.previous
      nav_items << "[< previous update](#{weekly_update.previous.web_url})"
    end

    if weekly_update.upcoming
      nav_items << "[next update >](#{weekly_update.upcoming.web_url})"
    end

    return unless nav_items.any?

    divider = ['---'] * nav_items.length

    <<~MARKDOWN

    | #{nav_items.join(' | ')} |
    | #{divider.join(' | ')} |

    MARKDOWN
  end

  def capacity_markdown
    capacity = Capacity.new(weekly_update.monday)
    total_percent = Markdown.percentage(capacity.team_total, capacity.engineer_days)
    frontend_percent = Markdown.percentage(capacity.frontend, capacity.frontend_days)
    backend_percent = Markdown.percentage(capacity.backend, capacity.backend_days)

    frontend_calculation = {
      days: capacity.frontend_days,
      PTO: capacity.frontend_deficit,
      Borrow: capacity.frontend_borrow
    }.select { _2 > 0 }

    backend_calculation = {
      days: capacity.backend_days,
      PTO: capacity.backend_deficit,
      Borrow: capacity.backend_borrow
    }.select { _2 > 0 }

    <<~MARKDOWN.chomp
    <details><summary>Capacity: #{total_percent} (#{frontend_percent} of ~frontend, #{backend_percent} of ~backend)</summary>

      _For the week of Monday, #{weekly_update.monday}._

      - ~frontend: max of #{Team.frontend.length} engineers, #{capacity.days_in_period} days in week
        - $`#{capacity.frontend}(days) = #{frontend_calculation.map { "#{_2}(#{_1})" }.join(' - ')}`$
      - ~backend: max of #{Team.backend.length} engineers, #{capacity.days_in_period} days in week
        - $`#{capacity.backend}(days) = #{backend_calculation.map { "#{_2}(#{_1})" }.join(' - ')}`$

      _Data from: [Monitor:Respond Group Calendar](https://calendar.google.com/calendar/u/0?cid=Z2l0bGFiLmNvbV8xbGMyZHFpbjFoMXQ2MHFoNnJmcjJjZTE5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t),
      via [Google App Scripts](https://script.google.com/macros/library/d/1LWrYPmjD-leQqLJlKmtuS8Nxyf62vWV-__XKguJyB6SSOW9W7LQSIOhW/9)_

      ---

    </details>
    MARKDOWN
  end

  def error_budget_markdown
    budget = ErrorBudget.new
    percentage = Markdown.percentage(budget.availability, precision: 2) if budget.availability

    <<~MARKDOWN.chomp
    <details><summary>Error budget: #{budget.status} #{budget.availability ? percentage : 'unknown'}</summary>

      <!-- If not 🟢, add detail (related issues, ongoing investigations, remediations, etc). -->

      _Data from: [Respond error budget dashboard](https://dashboards.gitlab.net/d/stage-groups-respond/stage-groups-respond-group-dashboard?orgId=1), [Respond error budget detail](https://dashboards.gitlab.net/d/stage-groups-detail-respond/stage-groups-respond-group-error-budget-detail?orgId=1&from=now-28d&to=now)_

      ---

    </details>
    MARKDOWN
  end

  def team_activities_markdown
    activity = TeamActivity.new(weekly_update, weekly_update.monday)

    contributions_by_object_type = activity.contributions
      .group_by(&:object)
      .map { |object, events| "#{events.uniq(&:url).count} #{object}" }

    activity_by_user = activity.contributions.group_by(&:username).map do |username, contributions|
      by_type = contributions.group_by { Markdown.labelify(_1.type) }.sort_by { _2.count }.reverse
      by_group = contributions.group_by { Markdown.labelify(_1.group) }.sort_by { _2.count }.reverse
      by_category = contributions.group_by { Markdown.labelify(_1.category) }.sort_by { _2.count }.reverse
      by_object = contributions.group_by { Markdown.labelify(_1.object) }.sort_by { _2.count }.reverse

      for_team, for_others = contributions.partition { _1.group == Team.group }
      count = contributions.count

      <<~MARKDOWN.chomp
      #### #{username} focus areas

      - Type: #{percentages_by_count(sort_by_count(by_type.to_h), count, truncate: true).join(', ')}
      - Group: #{percentages_by_count(sort_by_count(by_group.to_h), count, truncate: true).join(', ')}
      - Category: #{percentages_by_count(sort_by_count(by_category.to_h), count, truncate: true).join(', ')}
      - Work item: #{percentages_by_count(sort_by_count(by_object.to_h), count, truncate: true).join(', ')}

      <details><summary>~"#{Team.group}" contributions</summary>

      #{format_contributions(for_team)}

      </details>
      <details><summary>other contributions</summary>

      #{format_contributions(for_others)}

      </details>
      MARKDOWN
    end

    <<~MARKDOWN.chomp
    <details><summary>Contributions on: #{Markdown.list(contributions_by_object_type)}</summary><br/>

      _Contributions span from #{activity.start_date} to #{activity.end_date}._

      #{activity_by_user.join("\n\n")}

      _Data from: Scraped from activity page for each user. Activities which don't support labels are
      mostly ignored._

      ---

    </details>
    MARKDOWN
  end

  def format_contributions(contributions)
    lines = []

    contributions.group_by(&:object).each do |object, object_events|
      lines << "#{object}"

      object_events.group_by(&:api_path).each do |path, path_events|
        event = path_events.first

        action_counts = path_events.group_by(&:action).map do |action, action_events|
          "#{action} (#{action_events.count})"
        end.join(', ')

        lines << "- #{contribution_label(event.url, event.details&.title)}: #{action_counts}"
      end
    end

    lines.join("\n")
  end

  def contribution_label(url, title, label = nil)
    if url&.include?('merge_requests')
      url = "#{url}.html"
      titl = title.match(/^(?<title>.{0,45})(\s|$)/)[:title]
      link = "[#{titl}](#{url})"
    else
      link = Markdown.gitlab_link(url)
    end

    "#{link} #{Markdown.labelify(label)}"
  end

  def issue_distribution_markdown
    type_dist = IssueDistribution.new(plan.milestone_issues)
    type_dist.group { _1.type ? Markdown.labelify(_1.type) : 'w/o type' }
    type_dist.sub_group { _1.sub_type ? Markdown.labelify(_1.sub_type) : 'w/o sub-type' }

    epic_dist = IssueDistribution.new(plan.milestone_issues)
    epic_dist.group { _1.epic ? "gitlab-org&#{_1.epic.iid}+" : 'w/o epic' }
    epic_dist.sub_group do
      if _1.engineering_domains.any?
        _1.engineering_domains.map { |domain| Markdown.labelify(domain) }.join(' ')
      else
        'w/o eng label'
      end
    end

    <<~MARKDOWN.chomp

    <details><summary>#{plan.milestone.title} focus area distribution</summary>

      #{issue_distribution_table(type_dist)}

      #{issue_distribution_table(epic_dist)}

    </details>
    MARKDOWN
  end

  def issue_distribution_table(distribution)
    header = percentages_by_count(
      distribution.grouped,
      distribution.group_count,
      default_label: distribution.label
    )

    cells = distribution.sub_grouped.map do |sub_distribution|
      subsections = percentages_by_count(
        sub_distribution.grouped,
        sub_distribution.group_count,
        default_label: sub_distribution.label
      )

      Markdown.multiline_cell(subsections)
    end

    Markdown.table(header, [cells])
  end

  def progress_summary_markdown
    feature_completion = plan.by_feature.map do |feature, issues|
      if epic = issues.first.epic
        dashboard_link = "https://gitlab.com/groups/gitlab-org/-/issues/?label_name%5B%5D=#{CGI.escape(Team.group)}&milestone_title=#{plan.milestone.title}&not%5Blabel_name%5D%5B%5D=Planning%20Issue&epic_id=#{epic&.id}"
        epic_string = "from epic: gitlab-org&#{epic&.iid}+"
      else
        dashboard_link = "https://gitlab.com/groups/gitlab-org/-/issues/?label_name%5B%5D=#{CGI.escape(Team.group)}&milestone_title=#{plan.milestone.title}&not%5Blabel_name%5D%5B%5D=Planning%20Issue&epic_id=None"
        epic_string = "without an epic"
      end

      "#{percentage_by_weight(issues)} - of [scheduled issues](#{dashboard_link}) #{epic_string}"
    end

    <<~MARKDOWN.chomp
    <h4>~#{percentage_by_weight(plan.milestone_issues)} complete of #{plan.milestone.title} scheduled work</h4>

    #{feature_completion.map { '- ' + _1  }.join("\n")}

    _Percentage is a rough estimate and could be quite wrong. The calculation codifies `@syasonik`'s mental model for ~% completion, based on workflow labels, open MRs, and domain labels._

    MARKDOWN
  end

  def progress_detail_markdown
    tables = plan.by_feature.map do |feature, issues|
      header = "##### #{feature || 'Standalone issues'}  -  #{percentage_by_weight(issues)} complete"

      detailed_progress_table(header, issues)
    end

    tables.join('')
  end

  def detailed_progress_table(header, issues)
    rows = issues.map do |issue|
      title = "#{issue.web_url}+"
      key_labels = Markdown.label_list(issue.notable_labels)
      type = "Type: #{Markdown.label_list([issue.type, issue.sub_type])}"
      domains = "Domains: #{Markdown.label_list(issue.domains)}"
      assignees = Markdown.user_list(issue.assignees.map(&:username))
      ownership = Markdown.multiline_cell([assignees, issue.missing_assignee, "", issue.status])
      review_status = Markdown.multiline_cell(issue.review_status)
      percent_completion = "**#{Markdown.percentage(issue.completion_ratio)}**"

      if issue.errors.any?
        warnings = Markdown.multiline_cell(issue.errors.to_a, bulleted: true)
        warning_markdown = "<details><summary>Possible action items required</summary>#{warnings}</details>"
      end

      [
        Markdown.multiline_cell([title, key_labels, "", type, domains, warning_markdown]),
        Markdown.multiline_cell([ownership, review_status], divider: true),
        percent_completion
      ]
    end

    <<~MARKDOWN
    #{header}

    #{Markdown.table(['Issue', 'Status', "~% Complete"], rows)}

    MARKDOWN
  end

  def rollover_issues_markdown
    issues_by_milestone = plan.rollover_issues.group_by { _1.milestone.title }
    rollover_milestones = issues_by_milestone.keys.map { "%\"#{_1}\""}

    rollover_sections = issues_by_milestone.map do |milestone, issues|
      header = "**%\"#{milestone}\" rollover:**"
      list_items = issues.map { |issue| "- #{issue.web_url}+ `#{issue.assignee.username}`" }

      "#{header}\n#{list_items.join("\n")}"
    end

    <<~MARKDOWN.chomp
    <details><summary>#{plan.rollover_issues.count} Rollover issues from #{rollover_milestones.join(', ')}</summary><br/>

    #{rollover_sections.join("\n\n")}


    _Data from: GitLab REST API. Contains open issues which are
    in a prior milestone & assigned to a ~"#{Team.group}" engineer._

    ---

    </details>
    MARKDOWN
  end

  def team_links_markdown
    "- [Current Planning Issue](#{plan.url})"
  end

  def quick_actions_markdown
    <<~MARKDOWN
    /milestone %"#{plan.milestone.title}"

    MARKDOWN
  end

  def highlights_markdown
    issue = weekly_update.current
    return unless issue&.user_notes_count.to_i > 0

    threads = JankyCache.fetch("/projects/#{issue.project_id}/issues/#{issue.iid}/discussions") { weekly_update.gitlab_api.get(_1) }
    thread = threads.find { _1.notes.first.body.match?(/highlights/i) }
    return unless thread

    highlights = thread.notes[1..-1].map do |note|
      lines = note.body.split("\n")
      next note.body if lines.all? { _1.match?(/^\s*-\s+\b/) }

      lines.filter_map { '- ' + _1 unless _1.empty? }.join("\n")
    end

    highlights.join("\n")
  end

  def sort_by_count(grouped_issues)
    grouped_issues
      .sort_by { _2.count }
      .reverse
      .to_h
  end

  # @param issues_by_category Hash<String/Symbol, Array<Issue>>
  # @param total Integer
  def percentages_by_count(issues_by_category, total, truncate: false, default_label: 'unlabeled')
    if truncate
      big, small = issues_by_category.partition { |category, issues| issues.count >= 0.02 * total && category }
      other_issues = small.to_h.values.flatten

      if other_issues.any?
        default_label = 'other (<3% each)'
        issues_by_category = big.to_h.merge(nil => other_issues)
      end
    end

    issues_by_category.map do |category, issues|
      percent = Markdown.percentage(issues.count, total)

      "#{percent} #{category || default_label}"
    end
  end

  # @param issues Array<Issue>
  def percentage_by_weight(issues)
    numerator = issues.sum { |issue| issue.weight * issue.completion_ratio }
    denominator = issues.sum(&:weight)

    Markdown.percentage(numerator, denominator)
  end
end
