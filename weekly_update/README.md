## Overview

Generates a summary issue of Monitor:Respond group for the current week.
This can be configured to run for other groups as well.

## How it works

`weekly_update/generate.rb` acts as entrypoint for these scripts, relying
on environment vars defined via a scheduled pipeline.
A dry run is executed in every test stage in CI, issue creation only executes
if the GENERATE_WEEKLY_UPDATE flag is enabled.

Data is collected from GitLab issues, MRs, and user activity events. Data that
can't be found in GitLab is aggregated & exposed via a Google Apps Script.
This makes access to Google-based resources like calendars & spreadsheets simpler.

The team capacity and error budget sections require
some additional [configuration](#configuration).

## Running the weekly update

### On GitLab.com

This script is intended to run on Fridays, so the current week's issue can
be refreshed with data from the week. At the same time, an issue for the next
week is created to collect highlights as they occur.

1. Go to https://gitlab.com/gitlab-org/monitor/respond/-/pipeline_schedules
2. Manually run the pipeline described as "Generate detailed weekly update issue"
3. Go to https://gitlab.com/gitlab-org/monitor/respond/-/issues to see the new/updated issues!

### Locally

`GITLAB_API_PRIVATE_TOKEN="token" CI_PROJECT_ID="123456" GENERATE_WEEKLY_UPDATE=1 ruby ./weekly_update/generate.rb --dry-run`

While testing changes, use the `--dry-run` flag to avoid POST requests to the GitLab API.

The other [`options`](#options) below can be valuable in shortening the dev cycle,
as they enable the ability to focus on specific sections at one time.

#### Required env vars

- `CI_PROJECT_ID` | Env var of the project where the issue should be created
- `GITLAB_API_PRIVATE_TOKEN` | Env var of API token to be used to collect data & create the issue
- `GENERATE_WEEKLY_UPDATE` | Env var indicating whether

#### Options

- `--dry-run` | Prints the issue description to the log instead of creating an issue.
- `--skip-capacity` | Skips the 'Capacity' section of the issue description
- `--skip-error-budget` | Skips the 'Error budget' section of the issue description
- `--skip-team-activity` | Skips the 'Contributions' section of the issue description
- `--skip-issue-distribution` | Skips the 'Focus area distribution' section of the issue description
- `--skip-progress-summary` | Skips the 'Progress summary' section of the issue description
- `--skip-progress-detail` | Skips the 'Progress detail' section of the issue description
- `--skip-rollover` | Skips adding issues from previous milestones to the issue description
- `--skip-highlights` | Skips appending highlights from the issue discussion
- `--skip-navigation` | Skips adding links to the previous/next update issues
- `--skip-team-links` | Skips adding a link to the team's planning issue
- `--skip-quick-actions` | Skips adding milestone or issue attributes that change week to week

## Configuration

  - Modify `team.yml` to define team members, their specializations, and group info.
    - To include the 'Capacity' & 'Error Budget' sections, the aggregate values should be made available
      via a public API. For Monitor:Respond, this is accomplished with a Google Apps Script
      published as a web app, allowing access to the team's calendar.
      Reference: https://script.google.com/d/1jt8IzroTgc1CkY3c5v_ydq9CjbhiXlo34x24sPidNrr2Xqwy_u3_D4UJ/edit?usp=sharing
  - Modify `issue.rb` to alter math/conditions used to estimate issue completetion.
  - Modify `issue_templates/weekly-update.md` to alter the structure of the weekly update.
    This is also where you can control when certain data is collected & becomes available in the issue.
  - Modify `description.rb` to change how individual sections are generated & formatted.
  - Modify `milestone_plan.rb` to alter how the team's work is collected.
