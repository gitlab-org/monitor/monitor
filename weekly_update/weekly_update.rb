# Represents a weekly update issue
class WeeklyUpdate
  attr_reader :gitlab_api, :date, :project_id

  def initialize(gitlab_api, date, project_id)
    @gitlab_api = gitlab_api
    @project_id = project_id
    @date = date
    @threads = []
  end

  def previous
    @previous ||= find_issue(date.cweek - 1)
  end

  def current
    @current ||= find_issue(date.cweek)
  end

  def upcoming
    @upcoming ||= find_issue(date.cweek + 1)
  end

  def plan
    @plan ||= MilestonePlan.new(gitlab_api, date)
  end

  def create!(dry_run: false)
    return mock_issue(:CREATE) if dry_run

    gitlab_api.create_issue(
      project_id,
      title,
      { description: description(:CREATE) }
    ).tap { JankyCache.write("update-issue-#{date.cweek}", _1) }
  end

  def update!(dry_run: false)
    return mock_issue(:UPDATE) if dry_run

    gitlab_api.edit_issue(
      project_id,
      current.iid,
      { description: description(:UPDATE) }
    )
  end

  def comment!(dry_run: false)
    @threads.filter_map do |comment|
      next mock_comment(comment) if dry_run

      # Discussions API isn't supported with gitlab client
      gitlab_api.post(
        "/projects/#{project_id}/issues/#{current.iid}/discussions",
         body: { body: comment }
      )
    end
  end

  def add_thread(thread)
    @threads << thread
  end

  # Reference point for "start date" of the current week.
  # Used from capacity, team activity, etc
  def monday
    Date.commercial(date.year, date.cweek, 1)
  end

  private

  def title
    "Draft: Monitor:Respond Weekly Update - W#{date.cweek} - #{monday.strftime("%b-%d-%Y")}"
  end

  def description(action)
    body = current&.description || template

    Description.new(self, body).render(action)
  end

  def find_issue(week)
    JankyCache.fetch("update-issue-#{week}") {
      gitlab_api.issues(
        project_id,
        labels: ["OpsSection::Weekly-Update", Team.group],
        search: "W#{week}",
        in: "title"
      ).first
    }
  end

  def template
    File.read('.gitlab/issue_templates/weekly-update.md')
  end

  def mock_issue(action)
    MockIssue.new(-1, project_id, title, description(action))
  end

  def mock_comment(comment)
    MockDiscussion.new(
      -1,
      [ MockComment.new(comment) ]
    )
  end

  # Mock API response objects for dry-run
  MockComment = Struct.new(:body)
  MockDiscussion = Struct.new(:id, :notes)
  MockIssue = Struct.new(:iid, :project_id, :title, :description) do
    alias_method :id, :iid

    def web_url
      "/projects/#{project_id}/issues/#{iid}"
    end
  end
end
