<!-- 
Title should be:

Service Management:Respond ##.# Planning issue
-->

### Planning Boards

- [Service Management:Respond Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/1160198?milestone_title=15.10&label_name[]=group%3A%3Arespond)
- [Service Management:Respond Priority Board](https://gitlab.com/groups/gitlab-org/-/boards/5672901?milestone_title=16.1&label_name[]=group%3A%3Arespond)
- [Service Management:Respond - Who's working on what?](https://gitlab.com/groups/gitlab-org/-/boards/5333834?milestone_title=Any%20Milestone)
- [Prior Planning Issues](https://gitlab.com/gitlab-org/monitor/monitor/-/issues?sort=created_date&scope=all&state=all&label_name[]=Planning%20Issue)

### Goals for the milestone:

<!-- Replace these with the high-level goals for the milestone -->
*  Goal 1...
*  Goal 2...

### [OKRs for FY24-Q[-x-]](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?state=opened&label_name%5B%5D=group%3A%3Arespond)

<!-- Replace these with OKR's for this quarter -->
- https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/2912+
- https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/2913+


### Seeking Community Contributions

Issues that are great candidates for community contributions are listed **[here](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Seeking%20community%20contributions&label_name%5B%5D=group%3A%3Arespond&first_page_size=100)** and labeled ~"Seeking community contributions".   
We welcome all contributions and would be happy to support you along the way!

___
- [ ] Set the Milestone (current Milestone)
- [ ] Update the Milestone link for the Service Management:Respond Planning Board
- [ ] Set the Due Date for the end of the current Milestone

/label ~"group::respond" ~"devops::monitor" ~"section::ops" ~"Planning Issue" ~"type::ignore" ~"Category:Service Desk" 
/assign @francoisrose @kbychu
