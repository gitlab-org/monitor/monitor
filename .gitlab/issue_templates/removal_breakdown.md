<!-- 
Using this template: 
  - Add or remove any content as needed. This is meant to be a convenient starting point, not a required structure. The goal is to highlight & research unknowns or dependencies, align the team on a technical approach, raise any technical limitations early, and ensure all implementation tasks are represented by atomic issues.
  - Post any open questions as individual threads on the issue. When a decision is made, update the original comment with the answer and react with ✅. If relevant, update the issue description or any other related issues.
-->

This is the technical breakdown for <LINK TO REMOVAL EPIC/ISSUE>.

# Removal breakdown

### List of features which should not be available

<!-- When verifying the removal, which actions should no longer be available to the user? -->


### Dependencies, impact & blockers

<!-- Will a feature flag be used to facilitate removal? Or will code be removed directly? -->
<!-- Are there metrics which need to be deprecated before code changes can occur? 
       https://docs.gitlab.com/ee/development/service_ping/metrics_lifecycle.html#remove-a-metric -->
<!-- Do we need to accommodate the GraphQL deprecation process for any APIs? 
       https://docs.gitlab.com/ee/api/graphql/#deprecation-and-removal-process -->
<!-- How much usage does the feature currently have? 
     https://app.periscopedata.com/app/gitlab/737489/Respond-Group-Working-Dashboard
     https://app.periscopedata.com/app/gitlab/1032030/Respond-Group-Dashboard -->


### Relevant files

#### ~backend

#### ~frontend

#### ~documentation


### Issue breakdown & timeline

| Issue  | Notes | Milestone |
|--------|-------|-------|
|        |       |       |

<!-- 

For new issues: 
- [ ] Add section/stage/group/category/feature labels
- [ ] Add relevant frontend/backend labels as needed
- [ ] Set appropriate blocking linked issues
- [ ] Add to relevant epic
- [ ] Add detailed description, including: 
    - What: What is the goal of the issue?
    - Why: What is the motivation? Is there additional context which can be linked?
    - How: What is in scope? Out of scope? Anything blocking?

-->


# Definition of done

The following are recommended prior to starting removal:

- [ ] Detailed breakdown issue was posted to `#g_respond` with request for feedback
- [ ] All needed issues are created & linked to the appropriate epic
- [ ] All open threads are resolved or determined non-blocking


/label ~"group::respond" ~"devops::monitor" ~"section::ops" ~"work breakdown structure" ~"type::ignore" 
